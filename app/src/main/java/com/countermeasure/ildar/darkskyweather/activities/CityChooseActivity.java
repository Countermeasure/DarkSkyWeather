package com.countermeasure.ildar.darkskyweather.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.countermeasure.ildar.darkskyweather.R;
import com.countermeasure.ildar.darkskyweather.fragments.CityChooseFragment;

public class CityChooseActivity extends AppCompatActivity implements CityChooseFragment.onCityChangeListener {
    public static final String CITY_RESULT_LAT = "com.countermeasure.ildar.darskyweather.CITY_RESULT_LAT";
    public static final String CITY_RESULT_LNG = "com.countermeasure.ildar.darskyweather.CITY_RESULT_LNG";
    public static final String CITY_RESULT_NAME = "com.countermeasure.ildar.darskyweather.CITY_RESULT_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_choose);

        Toolbar toolbar = findViewById(R.id.city_choose_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCityChange(String lat, String lng, String name) {
        Intent intent = new Intent();
        intent.putExtra(CITY_RESULT_LAT, lat);
        intent.putExtra(CITY_RESULT_LNG, lng);
        intent.putExtra(CITY_RESULT_NAME, name);
        setResult(RESULT_OK, intent);
        finish();
    }
}