package com.countermeasure.ildar.darkskyweather.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.countermeasure.ildar.darkskyweather.App;
import com.countermeasure.ildar.darkskyweather.R;
import com.countermeasure.ildar.darkskyweather.models.GooglePlaces.city_autocomplete.CityAutocompleteModel;
import com.countermeasure.ildar.darkskyweather.models.GooglePlaces.city_autocomplete.Prediction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class CityAutoCompleteAdapter extends BaseAdapter implements Filterable {
    private final Context mContext;

    private List<Prediction> mResults;

    public CityAutoCompleteAdapter(Context context) {
        mContext = context;
        mResults = new ArrayList<Prediction>();
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public Prediction getItem(int index) {
        return mResults.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.city_choose_dropdown_item, parent, false);
        }
        Prediction city_model = getItem(position);
        ((TextView) convertView.findViewById(R.id.cityChoose_dropdown_item_textView)).setText(city_model.getDescription());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<Prediction> cities = findCities(mContext, constraint.toString());

                    filterResults.values = cities;
                    filterResults.count = cities.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null  && results.count > 0) {
                    mResults = (List<Prediction>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }

    private List<Prediction> findCities(Context mContext, String cityName) {
        List<Prediction> emptyArray = new ArrayList<>();
        if(cityName.length() < 3)
            return emptyArray;
        try {
            Response<CityAutocompleteModel> response = App.getGooglePlacesApi()
                    .getCityAutocompleteModel(cityName, App.LANG, App.CITY_CHOOSE_AUTOCOMPLETE_TYPES, App.GOOGLE_API_KEY)
                    .execute();
            CityAutocompleteModel model = response.body();
            if(!response.isSuccessful() || model == null) {
                Log.d(App.LOG_TAG, CityAutoCompleteAdapter.class.getSimpleName() + ": query is unsuccessful");
                Toast.makeText(mContext, mContext.getApplicationContext().getString(R.string.google_places_api_service_unavailable), Toast.LENGTH_LONG).show();
                return emptyArray;
            }
            if (model.getStatus().equals("OK")) {
                return model.getPredictions();
            } else {
                Log.d(App.LOG_TAG, CityAutoCompleteAdapter.class.getSimpleName() + ": query is unsuccessful. Status: " + model.getStatus());
            }
        } catch (IOException e) {
            Toast.makeText(mContext, "Google Places API недоступен. Отсутствует интернет подключение.", Toast.LENGTH_SHORT).show();
            Log.d(App.LOG_TAG, CityAutoCompleteAdapter.class.getSimpleName() + ": query failed");
            e.printStackTrace();
        }
        return emptyArray;
    }
}