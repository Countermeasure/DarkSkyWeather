package com.countermeasure.ildar.darkskyweather.api;

import com.countermeasure.ildar.darkskyweather.models.GooglePlaces.city_autocomplete.CityAutocompleteModel;
import com.countermeasure.ildar.darkskyweather.models.GooglePlaces.city_choose.PlaceModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GooglePlacesApi {
    @GET("/maps/api/place/autocomplete/json")
    Call<CityAutocompleteModel> getCityAutocompleteModel(
            @Query("input") String input,
            @Query("language") String language,
            @Query("types") String types,
            @Query("key") String key);

    @GET("/maps/api/place/details/json")
    Call<PlaceModel> getPlaceModel(
            @Query("placeid") String placeId,
            @Query("language") String language,
            @Query("types") String types,
            @Query("key") String key);
}
