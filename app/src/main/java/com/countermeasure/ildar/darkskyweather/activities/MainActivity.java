package com.countermeasure.ildar.darkskyweather.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.countermeasure.ildar.darkskyweather.App;
import com.countermeasure.ildar.darkskyweather.R;
import com.countermeasure.ildar.darkskyweather.models.DarkSky.weather.DataDaily;
import com.countermeasure.ildar.darkskyweather.models.DarkSky.weather.DataHourly;
import com.countermeasure.ildar.darkskyweather.models.DarkSky.weather.WeatherModel;
import com.countermeasure.ildar.darkskyweather.utils.DailyForecastAdapter;
import com.countermeasure.ildar.darkskyweather.utils.HourlyForecastAdapter;
import com.countermeasure.ildar.darkskyweather.utils.Utility;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public String latitude = "";
    public String longitude = "";
    public String city_name = "";
    private static boolean isPending = false;

    private RecyclerView.LayoutManager hourlyForecastLayoutManager;
    private RecyclerView.Adapter hourlyForecastAdapter;
    private RecyclerView.LayoutManager dailyForecastLayoutManager;
    private RecyclerView.Adapter dailyForecastAdapter;

    private RecyclerView dailyForecastRecyclerView;
    private RecyclerView hourlyForecastRecyclerView;
    private TextView cityTextView;
    private TextView tempTextView;
    private TextView feelsTempTextView;
    private TextView weatherTextView;
    private TextView humidityTextView;
    private TextView windTextView;
    private TextView pressureTextView;
    private ImageView weatherIconImageView;
    private FloatingActionButton fab;

    WeatherModel weatherModel;

    private ArrayList<DataHourly> hourlyForecastData;
    private ArrayList<DataDaily> dailyForecastData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = findViewById(R.id.mainActivity_fab);
        hourlyForecastRecyclerView = findViewById(R.id.mainActivity_hourlyForecast_RecyclerView);
        dailyForecastRecyclerView = findViewById(R.id.mainActivity_dailyForecast_RecyclerView);
        cityTextView = findViewById(R.id.mainActivity_cityName_TextView);
        tempTextView = findViewById(R.id.mainActivity_temperature_TextView);
        feelsTempTextView = findViewById(R.id.mainActivity_feelsLike_TextView);
        weatherTextView = findViewById(R.id.mainActivity_weatherText_TextView);
        humidityTextView = findViewById(R.id.mainActivity_humidity_TextView);
        windTextView = findViewById(R.id.mainActivity_wind_TextView);
        pressureTextView = findViewById(R.id.mainActivity_pressure_TextView);
        weatherIconImageView = findViewById(R.id.mainActivity_weatherIcon_ImageView);

        setSupportActionBar((Toolbar) findViewById(R.id.mainActivity_toolbar));

        hourlyForecastRecyclerView.setHasFixedSize(false);
        hourlyForecastRecyclerView.setNestedScrollingEnabled(false);
        hourlyForecastRecyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, DividerItemDecoration.HORIZONTAL));
        hourlyForecastLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        hourlyForecastRecyclerView.setLayoutManager(hourlyForecastLayoutManager);

        dailyForecastRecyclerView.setHasFixedSize(false);
        dailyForecastRecyclerView.setNestedScrollingEnabled(false);
        dailyForecastRecyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, DividerItemDecoration.VERTICAL));
        dailyForecastLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dailyForecastRecyclerView.setLayoutManager(dailyForecastLayoutManager);

        getSavedData();
        if(latitude.equals("") || longitude.equals("")) {
            startCityChooseActivity();
        } else if(weatherModel != null) {
            updateUi();
        } else {
            getWeather();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeather();
            }
        });
    }

    private void getSavedData() {
        SharedPreferences sharedPreferences = getSharedPreferences(App.SHARED_PREFERENCES, MODE_PRIVATE);
        if (sharedPreferences.contains(App.SHARED_PREFS_LATITUDE) && sharedPreferences.contains(App.SHARED_PREFS_LONGITUDE)) {
            latitude = sharedPreferences.getString(App.SHARED_PREFS_LATITUDE, "");
            longitude = sharedPreferences.getString(App.SHARED_PREFS_LONGITUDE, "");
            city_name = sharedPreferences.getString(App.SHARED_PREFS_CITY_NAME, "");
            String weatherModelString = sharedPreferences.getString(App.SHARED_PREFS_WEATHER_MODEL, "");
            if (!weatherModelString.equals("")) {
                weatherModel = new Gson().fromJson(weatherModelString, WeatherModel.class);
            }
        }
        if (weatherModel != null) {
            hourlyForecastData = weatherModel.getHourly().getData();
            dailyForecastData = weatherModel.getDaily().getData();
        } else {
            hourlyForecastData = new ArrayList<>();
            dailyForecastData = new ArrayList<>();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getWeather() {
        if(isPending) { return; }
        isPending = true;

        App.getDarkSkyApi().getWeatherData(App.DARK_SKY_API_KEY, latitude, longitude, App.LANG, App.UNITS)
                .enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                WeatherModel model = response.body();
                ResponseBody errorBody = response.errorBody();
                if(response.isSuccessful() && model != null) {
                    weatherModel = model;
                    updateUi();
                } else if (errorBody != null) {
                    try {
                        Toast.makeText(MainActivity.this, errorBody.string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                isPending = false;
            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                isPending = false;
                Toast.makeText(MainActivity.this, "Ошибка получения данных о погоде. Отсутствует интернет подключение.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    private void updateUi() {
        cityTextView.setText(city_name);
        tempTextView.setText(getString(R.string.temperature, weatherModel.getCurrently().getTemperature()));
        feelsTempTextView.setText(getString(R.string.feels_like, weatherModel.getCurrently().getApparentTemperature()));
        weatherTextView.setText(weatherModel.getCurrently().getSummary());
        humidityTextView.setText(getString(R.string.humidity, weatherModel.getCurrently().getHumidity() * 100));
        windTextView.setText(getString(R.string.wind_speed, weatherModel.getCurrently().getWindSpeed()));
        pressureTextView.setText(getString(R.string.pressure, Utility.hectopascalToSI(weatherModel.getCurrently().getPressure())));
        weatherIconImageView.setImageResource(Utility.getIconResourceId(MainActivity.this, weatherModel.getCurrently().getIcon()));

        hourlyForecastData = weatherModel.getHourly().getData();
        hourlyForecastAdapter = new HourlyForecastAdapter(MainActivity.this, hourlyForecastData);
        hourlyForecastRecyclerView.setAdapter(hourlyForecastAdapter);

        dailyForecastData = weatherModel.getDaily().getData();
        dailyForecastAdapter = new DailyForecastAdapter(MainActivity.this, dailyForecastData);
        dailyForecastRecyclerView.setAdapter(dailyForecastAdapter);
    }

    private void startCityChooseActivity() {
        Intent intent = new Intent(this, CityChooseActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null && latitude.equals("") && longitude.equals("")) {
            finish();
        } else if (data == null || resultCode != RESULT_OK) {
            return;
        } else if(requestCode == 1) {
            latitude = data.getStringExtra(CityChooseActivity.CITY_RESULT_LAT);
            longitude = data.getStringExtra(CityChooseActivity.CITY_RESULT_LNG);
            city_name = data.getStringExtra(CityChooseActivity.CITY_RESULT_NAME);

            getWeather();
        }
    }

    @Override
    protected void onPause() {
        if(weatherModel != null && !latitude.equals("") && !longitude.equals("")) {
            SharedPreferences sharedPreferences = getSharedPreferences(App.SHARED_PREFERENCES, MODE_PRIVATE);
            sharedPreferences.edit()
                    .putString(App.SHARED_PREFS_LATITUDE, latitude)
                    .putString(App.SHARED_PREFS_LONGITUDE, longitude)
                    .putString(App.SHARED_PREFS_CITY_NAME, city_name)
                    .apply();
            Gson gson = new Gson();
            String json = gson.toJson(weatherModel);
            sharedPreferences.edit()
                    .putString(App.SHARED_PREFS_WEATHER_MODEL, json)
                    .apply();
        }
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_change_city) {
            startCityChooseActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}