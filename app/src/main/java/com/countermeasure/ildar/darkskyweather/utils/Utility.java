package com.countermeasure.ildar.darkskyweather.utils;

import android.content.Context;

import com.countermeasure.ildar.darkskyweather.R;

import java.text.SimpleDateFormat;

public class Utility {
    private Utility() {}

    public static double hectopascalToSI(double val) {
        return val * 0.750064d;
    }

    public static String getTime(long value) {
        return new SimpleDateFormat("HH:mm").format(new java.util.Date(value * 1000L));
    }

    public static String getDate(long value) {
        return new SimpleDateFormat("dd MMMM").format(new java.util.Date(value * 1000L));
    }

    public static int getIconResourceId(Context mContext, String iconName) {
        switch (iconName) {
            case "clear-day":
                return R.drawable.wi_day_sunny;
            case "clear-night":
                return R.drawable.wi_night_clear;
            case "rain":
                return R.drawable.wi_rain;
            case "snow":
                return R.drawable.wi_snow;
            case "sleet":
                return R.drawable.wi_sleet;
            case "wind":
                return R.drawable.wi_windy;
            case "fog":
                return R.drawable.wi_fog;
            case "cloudy":
                return R.drawable.wi_cloudy;
            case "partly-cloudy-day":
                return R.drawable.wi_day_sunny_overcast;
            case "partly-cloudy-night":
                return R.drawable.wi_night_alt_partly_cloudy;
            default:
                return R.drawable.wi_na;
        }
    }
}
