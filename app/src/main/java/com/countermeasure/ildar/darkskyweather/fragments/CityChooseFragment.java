package com.countermeasure.ildar.darkskyweather.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.countermeasure.ildar.darkskyweather.App;
import com.countermeasure.ildar.darkskyweather.R;
import com.countermeasure.ildar.darkskyweather.models.GooglePlaces.city_autocomplete.Prediction;
import com.countermeasure.ildar.darkskyweather.models.GooglePlaces.city_choose.PlaceModel;
import com.countermeasure.ildar.darkskyweather.utils.CityAutoCompleteAdapter;
import com.countermeasure.ildar.darkskyweather.utils.DelayAutoCompleteTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityChooseFragment extends Fragment {
    private DelayAutoCompleteTextView act;
    private onCityChangeListener cityChangeListener;


    public interface onCityChangeListener {
        void onCityChange(String lat, String lng, String name);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            cityChangeListener = (onCityChangeListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onCityChangeListener");
        }
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_city_choose, container, false);

        act = rootView.findViewById(R.id.delayAutoCompleteTextView);
        act.setThreshold(3);

        CityAutoCompleteAdapter autoCompleteAdapter = new CityAutoCompleteAdapter(getContext());

        act.setAdapter(autoCompleteAdapter);
        act.setLoadingIndicator((ProgressBar) rootView.findViewById(R.id.progress_bar));
        act.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Prediction city_prediction = (Prediction) parent.getItemAtPosition(position);
                act.setText(city_prediction.getDescription());

                Log.d(App.LOG_TAG, CityChooseFragment.class.getSimpleName() + ": query start. Place_id = " + city_prediction.getPlaceId());
                App.getGooglePlacesApi()
                        .getPlaceModel(city_prediction.getPlaceId(), App.LANG, App.CITY_CHOOSE_PLACE_FIELDS, App.GOOGLE_API_KEY)
                        .enqueue(new Callback<PlaceModel>() {
                            @Override
                            public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                                PlaceModel model = response.body();
                                if (!response.isSuccessful() || model == null) {
                                    Log.d(App.LOG_TAG, this.getClass().getName() + ": query is unsuccessful");
                                    if (getContext() != null) {
                                        Toast.makeText(getContext(), getContext().getString(R.string.google_places_api_service_unavailable), Toast.LENGTH_SHORT).show();
                                    }
                                } else if (!model.getStatus().equals("OK")) {
                                    Log.d(App.LOG_TAG, CityChooseFragment.class.getSimpleName() + ": Status = " + model.getStatus());
                                } else {
                                    Log.d(App.LOG_TAG, CityChooseFragment.class.getSimpleName() + ": query status = OK");
                                    cityChangeListener.onCityChange(
                                            String.valueOf(model.getResult().getGeometry().getLocation().getLat()),
                                            String.valueOf(model.getResult().getGeometry().getLocation().getLng()),
                                            model.getResult().getFormattedAddress());
                                }
                            }

                            @Override
                            public void onFailure(Call<PlaceModel> call, Throwable t) {
                                Log.d(App.LOG_TAG, this.getClass().getSimpleName() + ": query is failed");
                                t.printStackTrace();
                                if (getContext() != null) {
                                    Toast.makeText(getContext(), getContext().getString(R.string.google_places_api_service_unavailable), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
        return rootView;
    }
}
