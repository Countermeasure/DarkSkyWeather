package com.countermeasure.ildar.darkskyweather.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.countermeasure.ildar.darkskyweather.App;
import com.countermeasure.ildar.darkskyweather.R;
import com.countermeasure.ildar.darkskyweather.models.DarkSky.weather.DataHourly;

import java.util.ArrayList;

public class HourlyForecastAdapter extends RecyclerView.Adapter<HourlyForecastAdapter.ViewHolder> {
    ArrayList<DataHourly> hourlyData;
    Context mContext;

    public HourlyForecastAdapter(Context context, ArrayList<DataHourly> hourlyForecastData) {
        if (hourlyForecastData == null) {
            hourlyForecastData = new ArrayList<>();
        }
        this.hourlyData = hourlyForecastData;
        this.mContext = context;
    }

    @NonNull
    @Override
    public HourlyForecastAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hourly_forecast_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HourlyForecastAdapter.ViewHolder holder, int position) {
        DataHourly dh = hourlyData.get(position);
        if(dh != null) {
            holder.time.setText(Utility.getTime(hourlyData.get(position).getTime()));
            holder.temperature.setText(mContext.getString(R.string.temperature, dh.getTemperature()));
            holder.icon.setImageResource(Utility.getIconResourceId(mContext, dh.getIcon()));
            holder.precipProb.setText(mContext.getString(R.string.precipProbability, hourlyData.get(position).getPrecipProbability() * 100));
        }
    }

    @Override
    public int getItemCount() {
        return hourlyData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView time;
        public ImageView icon;
        public TextView temperature;
        public TextView precipProb;

        public ViewHolder(View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.hourly_forecast_item_time_textView);
            icon = itemView.findViewById(R.id.hourly_forecast_item_icon_imageView);
            precipProb = itemView.findViewById(R.id.hourly_forecast_item_precipProb_textView);
            temperature = itemView.findViewById(R.id.hourly_forecast_item_temp_textView);
        }
    }
}
