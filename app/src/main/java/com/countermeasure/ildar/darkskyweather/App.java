package com.countermeasure.ildar.darkskyweather;

import android.app.Application;
import android.content.Context;

import com.countermeasure.ildar.darkskyweather.api.DarkSkyApi;
import com.countermeasure.ildar.darkskyweather.api.GooglePlacesApi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    private static GooglePlacesApi googlePlacesApi;
    private static DarkSkyApi darkSkyApi;
    private static Retrofit retrofit;

    public static final String LOG_TAG = "DARKSKY_WEATHER_LOG_TAG";
    public static final String GOOGLE_API_KEY = "AIzaSyCh2c311mpE-Jdp-ESfeKP1tJzg5hjIVZM";
    public static final String DARK_SKY_API_KEY = "517e65a14c49b4c7ab5ad4f708aaaa41";
    public static final String LANG = "ru";
    public static final String UNITS = "si";
    public static final String CITY_CHOOSE_AUTOCOMPLETE_TYPES = "(cities)";
    public static final String CITY_CHOOSE_PLACE_FIELDS = "geometry/location";

    public static final String SHARED_PREFERENCES = "darkskyweather_sp";
    public static final String SHARED_PREFS_LATITUDE = "darkskyweather_sp.latitude";
    public static final String SHARED_PREFS_LONGITUDE = "darkskyweather_sp.longitude";
    public static final String SHARED_PREFS_CITY_NAME = "darkskyweather_sp.city_name";
    public static final String SHARED_PREFS_WEATHER_MODEL = "darkskyweather_sp.weather_model";

    @Override
    public void onCreate() {
        super.onCreate();
        final String GOOGLE_API_URL = "https://maps.googleapis.com";
        final String DARK_SKY_API_URL = "https://api.darksky.net";

        Retrofit mRetrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(GOOGLE_API_URL)
                .build();

        googlePlacesApi = mRetrofit.create(GooglePlacesApi.class);

        mRetrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(DARK_SKY_API_URL)
                .build();
        darkSkyApi = mRetrofit.create(DarkSkyApi.class);
    }

    public static GooglePlacesApi getGooglePlacesApi() {
        return googlePlacesApi;
    }

    public static DarkSkyApi getDarkSkyApi() {
        return darkSkyApi;
    }
/*
    public static double hectopascalToSI(double val) {
        return val * 0.750064d;
    }

    public static String getTime(long value) {
        return new SimpleDateFormat("HH:mm").format(new java.util.Date(value * 1000L));
    }

    public static String getDate(long value) {
        return new SimpleDateFormat("dd MMMM").format(new java.util.Date(value * 1000L));
    }

    public static int getIconResourceId(Context mContext, String iconName) {
        switch (iconName) {
            case "clear-day":
                return R.drawable.wi_day_sunny;
            case "clear-night":
                return R.drawable.wi_night_clear;
            case "rain":
                return R.drawable.wi_rain;
            case "snow":
                return R.drawable.wi_snow;
            case "sleet":
                return R.drawable.wi_sleet;
            case "wind":
                return R.drawable.wi_windy;
            case "fog":
                return R.drawable.wi_fog;
            case "cloudy":
                return R.drawable.wi_cloudy;
            case "partly-cloudy-day":
                return R.drawable.wi_day_sunny_overcast;
            case "partly-cloudy-night":
                return R.drawable.wi_night_alt_partly_cloudy;
            default:
                return R.drawable.wi_na;
        }
    }
*/
}