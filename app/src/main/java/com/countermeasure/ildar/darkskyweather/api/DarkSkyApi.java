package com.countermeasure.ildar.darkskyweather.api;

import com.countermeasure.ildar.darkskyweather.models.DarkSky.weather.WeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DarkSkyApi {
    @GET("/forecast/{api_key}/{latitude},{longitude}")
    Call<WeatherModel> getWeatherData(
            @Path("api_key") String api_key,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude,
            @Query("lang") String language,
            @Query("units") String units);
}
//55.801260,49.244842

//https://api.darksky.net/forecast/517e65a14c49b4c7ab5ad4f708aaaa41/55.801080,49.244764?lang=ru&units=si