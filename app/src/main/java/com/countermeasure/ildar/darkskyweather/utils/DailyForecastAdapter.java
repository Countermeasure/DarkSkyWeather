package com.countermeasure.ildar.darkskyweather.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.countermeasure.ildar.darkskyweather.App;
import com.countermeasure.ildar.darkskyweather.R;
import com.countermeasure.ildar.darkskyweather.models.DarkSky.weather.DataDaily;

import java.util.ArrayList;

import okhttp3.internal.Util;

public class DailyForecastAdapter extends RecyclerView.Adapter<DailyForecastAdapter.ViewHolder> {
    ArrayList<DataDaily> dailyData;
    Context mContext;

    public DailyForecastAdapter(Context context, ArrayList<DataDaily> dailyForecastData) {
        if(dailyForecastData == null) {
            dailyForecastData = new ArrayList<>();
        }
        this.dailyData = dailyForecastData;
        this.mContext = context;
    }

    @NonNull
    @Override
    public DailyForecastAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.daily_forecast_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DailyForecastAdapter.ViewHolder holder, int position) {
        DataDaily dd = dailyData.get(position);
        if(dd != null) {
            holder.date.setText(Utility.getDate(dd.getTime()));
            holder.minTemp.setText(mContext.getString(R.string.temperature, dd.getTemperatureMin()));
            holder.maxTemp.setText(mContext.getString(R.string.temperature, dd.getTemperatureMax()));
            holder.precipProb.setText(mContext.getString(R.string.precipProbability, dd.getPrecipProbability() * 100));
            holder.icon.setImageResource(Utility.getIconResourceId(mContext, dd.getIcon()));
        }
    }

    @Override
    public int getItemCount() {
        return dailyData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView date;
        TextView maxTemp;
        TextView minTemp;
        TextView precipProb;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.daily_forecast_date_textView);
            maxTemp = itemView.findViewById(R.id.daily_forecast_item_maxTemp_textView);
            minTemp = itemView.findViewById(R.id.daily_forecast_item_minTemp_textView);
            precipProb = itemView.findViewById(R.id.daily_forecast_precipProb_textView);
            icon = itemView.findViewById(R.id.daily_forecast_item_icon_imageView);

        }
    }
}
